<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Normalizer;

class CidadeController extends Controller
{
    // Dados fictícios de cidades e estados
    private $dadosCidadesEstados = [
        "Acrelândia" => "AC",
        "Assis Brasil" => "AC",
        "Brasiléia" => "AC",
        "Bujari" => "AC",
        "Capixaba" => "AC",
        "Cruzeiro do Sul" => "AC",
        "Epitaciolândia" => "AC",
        "Feijó" => "AC",
        "Jordão" => "AC",
        "Mâncio Lima" => "AC",
        "Manoel Urbano" => "AC",
        "Marechal Thaumaturgo" => "AC",
        "Plácido de Castro" => "AC",
        "Porto Acre" => "AC",
        "Porto Walter" => "AC",
        "Rio Branco" => "AC",
        "Rodrigues Alves" => "AC",
        "Santa Rosa do Purus" => "AC",
        "Sena Madureira" => "AC",
        "Senador Guiomard" => "AC",
        "Tarauacá" => "AC",
        "Xapuri" => "AC",
        // Adicione mais dados conforme necessário
    ];

    public function buscarCidadesEstados(Request $request)
    {
        $termo = Normalizer::normalize(mb_strtolower($request->input('termo', '')), Normalizer::FORM_D);

        $resultados = collect($this->dadosCidadesEstados)->filter(function ($value, $key) use ($termo) {
            $keySemAcentos = Normalizer::normalize(mb_strtolower($key), Normalizer::FORM_D);
            return mb_stripos($keySemAcentos, $termo) === 0;
        });

        return response()->json($resultados, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
